import React, { useState } from "react";
import Sound from "react-sound";

const PlaySound = (
    handleSongLoading,
    handelSongPlaying,
    handleSongFinishedPlaying
) => {
    const [isPlaying, setIsPlaying] = useState(true);

    return (
        <div className="bg-black">
            <button onClick={() => setIsPlaying(!isPlaying)}>{!isPlaying ? 
                <img
                    src={"/sound_off.gif"}
                    style={{
                    width: 45,
                    padding: 5,
                    }}
                />
                :   
                <img
                    src={"/sound.gif"}
                    style={{
                    width: 35,
                    padding: 5,
                    }}
                /> 
                }
            </button>
            <Sound 
                url="/music.mp3"
                playStatus={
                    isPlaying ? Sound.status.PLAYING : Sound.status.STOPPED
                }
                playFromPosition={300}
                onLoading={handleSongLoading}
                onPlaying={handelSongPlaying}
                onFinishedPlaying={handleSongFinishedPlaying}
            />
        </div>
    );
};

export default PlaySound;