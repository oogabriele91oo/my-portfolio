const portfolio = {
  // 
  resume: {
    projects: {
      title: "PROJECTS",
      list: [
        {
          title: "Japan Travel Blog",
          description:
            "Japan Travel Blog is created with React.JS, Wordpress-API and some React-library. It is fully functional blog with category and call to static page.The blog was also implemented with a modal containing author information and background music.",
        },
        {
          title: "Ghibliflix",
          description:
            "Ghibliflix is created with React.JS, Studio Ghibli-API and some React-library. is a fully functional commemorative app honoring Studio Ghibli. Allows viewing of the entire filmography of the Ghibli studio with the details of each film. The application is enriched with a search component and basic animations.",
        },
        {
          title: "Movie App",
          description:
            "Movie App is created with React.JS, MovieDB-API and some React-library. is a movie app with a list of film, filter for category and search by title component.",
        },
        {
          title: "Map Project",
          description:
            "I have developed a single page app to view a map and be able to insert markers indicating the position. Within the code I have implemented several maps and solutions that can be shown or changed from the default one (google maps) to ensure maximum usability in case of problems in loading the default one. The map can be integrated into countless applications and / or solutions.",
        },
        {
          title: "Lovely Scrubs Store",
          description:
            "this Medical scrubs ecommerce was developed with Laravel and MySQL. A dashboard has been created for the management of products, categories and orders. On the user side we find the home with all the products with carousel and different views, the product detail with the addition to the cart and the categories section with the list of categories and the division of products by category.",
        },
        {
          title: "Fake Amazon",
          description:
            "This Online Store fo Electronic Devices was developed with PHP and SQL databases according to the MVC pattern. In the navbar there is the list / link of the product categories. It is possible to add the products to the cart and checkout. There is also the registration form for new users and the login form necessary to proceed with the order.",
        },
      ],
    },
    info: {
      title: "INFO",
      name: "Gabriele Serafini",
      age: " ,30",
      list: [
        ["Email -", "gabribb91@gmail.com"],
        ["LinkedIn - ", "gabriele-serafini-a2050a1ba"],
        ["Bitbucket - ", "oogabriele91oo"],
        ["Github - ", "gabrieleserafini"],
      ],
    },
    education: {
      title: "EDUCATION",
      list: [
        {
          company: "Epicode School, < Italy, Rome(RM) />",
          course:
            "Advance Certification, Full-Stack Web Development",
          range: " 2022 - ",
        },
        {
          company: "Lacerba, < Italy, Milan(MI) />",
          course:
            "Course in User Experience Design and Ux Landing Page Design",
          range: " 2022 - ",
        },
        {
          company: "Lacerba, < Italy, Milan(MI) /> ",
          course:
            "Course in Big Data and Machine Learning",
          range: " 2022 - ",
        },
        {
          company: "Google, < USA /> ",
          course: "Certification in Digital Marketing",
          range: "2021 : 2022 - ",
        },
        {
          company: "I.T.E. 'A.Gentili', < Italy, Macerata(MC) />",
          course: "High School Degree as Accountant Programmer",
          range: "2005 : 2010 - ",
        },
      ],
    },
    experience: {
      title: "EXPERIENCE",
      list: [
        {
          range: "2020 : 2022 -",
          company: "S.S. Multi Service < Italy, Aprilia(LT) />",
          title: " Webmaster",
          description: ` - choose which software to use when building a website, approve the content used in the site and troubleshoot technical problems.`,
        },
        {
          range: "2018 : 2020 - ",
          company: "Foster Company < Italy, Rome(RM) /> ",
          title: "Store Manager ",
          description: ` - In addition to the responsibilities of an e-commerce specialist, <br />-
          I was offered the management of a physical store located in Rome. <br />-
          In that period to implement sales I started developing simple web applications through CMR to implement the visibility and sales of the store.`,
        },
        {
          range: "2015 : 2027 - ",
          company: "Foster Company < Italy, Verona(VR) />",
          title: "E-Commerce Specialist",
          description: ` - 
          Development of online marketing plans within the company  <br />-
          With a focus on user experience and customer movements within the company e-commerce <br />-
          With my work I have increased the conversion rate by 30% and the sales by about 20% year on year. <br /> -
          I also designed multimedia content and banners.`,
        },
      ],
    },
    bio: {
      title: "BIO",
      description: `
      Hey, I'm Gabriele Serafini, A Junior Web Developer, Currently living
          near Rome~Italy. If you are looking for a developer, I am currently
          exploring job opportunities, feel free to contact me.
          <br/><br/>I love programming, I grew up with my father, who was also a programmer, 
          where we assembled PCs in our free time. I am passionate about
          programing and in the last years, i have study alot of
          projects, frameworks, and languages. In my spare time i usually
          program, search new technologies and spend time with my family( expecially with N.J., my dog).
          My next goal is to specialize in cloud development.
      `,
    },
    skills: {
      title: "SKILLS",
      list: [
        "HTML",
        "CSS",
        "Bootstrap",
        "Javascript",
        "JQuery",
        "React JS",
        "Node Js",
        "PHP",
        "SQL",
        "Laravel",
        "Firebase",
      ],
    },
    title: "Junior Full-Stack Web Developer",
  },
  layout: {
    right: [
      {
        url: "/resume",
        label: "C. V . ",
        link: true,
      },
      {
        url: "mailto:gabribb91@gmail.com",
        label: "Email",
        link: false,
      },
      {
        url: "https://bitbucket.org/oogabriele91oo/",
        label: "Bitbucket",
      },
      {
        url: "https://www.linkedin.com/in/gabriele-serafini-a2050a1ba/",
        label: "LinkedIn",
      },
    ],
    left: [
      {
        url: "/about-me",
        label: "About",
      },
      {
        url: "/contact-me",
        label: "Contact",
      },
      {
        url: "/projects",
        label: "Projects",
      },
      {
        url: "/",
        label: "Home",
      },
    ],
  },

  contact: {
    helmet: {
      title: "Gabriele Serafini - Contact Me",
    },
    title: "GET IN TOUCH 🏃",
    description:
      "I am currently living near Rome(Aprilia), Italy and exploring opportunities around me (or why not, even outside my area if the offer is interesting) and my inbox is always open. Whether you have a question or just want to say hi, I’ll try my best to get back to you!",
    btn: {
      label: "Say Hi 👋",
      url: "https://www.linkedin.com/in/gabriele-serafini-a2050a1ba/",
    },
    ending: "Or gabribb91@gmail.com",
  },
  //
  About: {
    helmet: {
      title: "Gabriele Serafini - About Me",
    },
    title: "BIO",
    social: {
      label: "🏃 Follow me on ",
      links: [
        { label: "Linkedin", url: "https://www.linkedin.com/in/gabriele-serafini-a2050a1ba/" },
        { label: "Bitbucket", url: "https://bitbucket.org/oogabriele91oo/" },
      ],
    },
    description: [
      ` A tireless workaholic. 
              Both in work and in life I am a perfectionist on a manic level. 
              I've worked a lot for the last year for been the 
              best version of myself.
              Currently living near Rome(Aprilia)~Italy.`,
      ` The Abundant energy I have fuels me in the pursuit of various
              interests, hobbies, areas of studies, and artistic endeavors. I am
              a fast learner who adapts quickly to change and eager to learn new
              methods and procedures, whether it is a cultural change or a
              work-related one`,
      `I am an adventurer, I want to travel the world, I am fascinated by
              the beauty of the Earth and the infinite possibilities that the word offers.`,
      `  I'm usually focused on : <br /> 🎛️ Studying and improving myself <br />
              🌐 Developing new Project <br />✨ Developing
              clean code. <br />
              🧰 Building Elegant Solutions <br />
              ⏱️ Efficiency 🎯 Consistency <br />
              ♻️ Reusability <br />
              ⚙️ Speed up my Critical Thinking <br />
              📋 Respect Standards <br />`,
    ],
  },
  // 
  home: {
    showModel: true,
    helmet: {
      title: "Gabriele Serafini - Welcome to My Dev-folio",
    },
    title: ["Gabriele", "Serafini"],
    subTitle: "Full Stack Web Developer Jr",
    description: `Coding Addicted`,
  },
  // 
  projects: [
    {
      technology: ["React JS", "WordPress-API"],
      slug: "react-wordpress-api",
      title: "Japan-Travel-Blog",
      description:
        "Japan Travel Blog is created with React.JS, Wordpress-API and some React-library. It is fully functional blog with category and call to static page.The blog was also implemented with a modal containing author information and background music.",
      image: "/images/blog.jpg",
      link: [
        {
          text: "Bitbucket",
          url: "https://bitbucket.org/oogabriele91oo/react-wordpress-api/src/main/blog-react-wp/",
        },
        {
          text: "Read More about Wordpress API",
          url: "https://developer.wordpress.org/rest-api/",
        },
      ],
    },
    {
      technology: ["React.js", "MovieDB-API"],
      slug: "movie",
      title: "Movie App",
      description:
        "Movie App is created with React.JS, MovieDB-API and some React-library. is a movie app with a list of film, filter for category and search by title component.",
      image: "/images/netfake.jpg",
      link: [
        {
          text: "Bitbucket",
          url: "https://bitbucket.org/oogabriele91oo/fake-netflix/src/main/",
        },
        {
          text: "Read More about MovieDB-API",
          url: "https://developers.themoviedb.org/3/getting-started/introduction",
        },
      ],
    },
    {
      technology: ["React.js", "Studio Ghibli-API"],
      slug: "film",
      title: "Ghibliflix",
      description:
        "Ghibliflix is created with React.JS, Studio Ghibli-API and some React-library. is a fully functional commemorative app honoring Studio Ghibli. Allows viewing of the entire filmography of the Ghibli studio with the details of each film. The application is enriched with a search component and basic animations.",
      image: "/images/ghibliflix.jpg",
      link: [
        {
          text: "Bitbucket",
          url: "https://bitbucket.org/oogabriele91oo/ghiblifix/src/main/ghibliflix/",
        },
        {
          text: "Read More about Studio Ghibli API",
          url: "https://ghibliapi.herokuapp.com/",
        },
      ],
    },
    {
      technology: ["React.js", "Azure Map-API", "Google Map-API", "Mapbox-API"],
      slug: "maps",
      title: "Maps",
      description:
        "Single page app to view a map and be able to insert markers indicating the position. Within the code I have implemented several maps and solutions that can be shown or changed from the default one (google maps) to ensure maximum usability in case of problems in loading the default one.",
      image: "/images/map.jpg",
      link: [
        {
          url: "https://bitbucket.org/oogabriele91oo/map-repository/src/main/",
          text: "Bitbucket",
        },
        {
          url: "https://developers.google.com/maps/documentation/javascript/react-map",
          text: "Read More about Maps JavaScript API",
        },
      ],
    },
    {
      technology: ["Laravel", "MySQL"],
      slug: "laravel-ecommerce",
      title: "Scrubs Store",
      description:
        "Scrubs Ecommerce developed with Laravel and MySQL. A dashboard has been created for the management of products, categories and orders. On the user side we find the home with all the products with carousel and different views, the product detail with the addition to the cart.",
      image: "/images/LovelyScrubs.jpg",
      link: [
        {
          url: "#",
          text: "Bitbucket",
        },
        {
          url: "https://laravel.com/docs/9.x",
          text: "Read More about Laravel",
        },
      ],
    },
    {
      technology: ["PHP", "SCSS", "MariaDB"],
      slug: "php-ecommerce",
      title: "PHP Shop",
      description:
        "This Online Store of electronic devices was developed with PHP and SQL databases according to the MVC pattern. In the navbar there is the list / link of the product categories. It is possible to add the products to the cart and checkout. There is also the registration form for new users and the login form necessary to proceed with the order.",
      image: "/images/FakeAmazon.jpg",
      link: [
        {
          url: "https://github.com/gabrieleserafini/phpEcommerce",
          text: "GitHub",
        },
        {
          url: "https://www.php.net/",
          text: "Read More about PHP",
        },
      ],
    },
  ],
};
export default portfolio;
