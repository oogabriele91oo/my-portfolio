import emailjs from '@emailjs/browser';
import React, { useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import { FaWhatsapp } from 'react-icons/fa';
import { FiMail } from 'react-icons/fi';
import { RiMessengerLine } from 'react-icons/ri';
import ContactItem from './ContactItem';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Error from './Error';

const Contact = ({ snap }) => {
  const notify = () => toast.success('Mail send successfully!')
  const form = useRef();
  const { register, handleSubmit, formState: { errors } } = useForm({
    mode: "all",
  });
  const [rangeValue, setRangeValue] = useState(3);

  const sendEmail = (e) => {
    emailjs.sendForm('service_1e76akw', 'template_czdfv8s', form.current, 'U7JJdf3f0XNJprWar');
    notify();
    form.current.reset();
  };

  return (
    <section id='contact'>

      <div className='w-3/4 lg:w-3/5 m-auto flex flex-col md:flex-row gap-16 lg:gap-20'>
        <div className='flex flex-col gap-6 bg-liBg dark:bg-transparent'>
          <ContactItem header='Email' contact='gabribb91@gmail.com' href='mailto:gabribb91@gmail.com'>
            <FiMail className='text-liBg group-hover:text-liSec dark:text-primary dark:group-hover:text-primary m-auto text-2xl mb-4' />
          </ContactItem>
          <ContactItem header='Messenger' contact='Gabriele Serafini' href='https://m.me/ludvig.lindahl.1'>
            <RiMessengerLine className='text-liBg group-hover:text-liSec dark:text-primary dark:group-hover:text-primary m-auto text-2xl mb-4' />
          </ContactItem>
          <ContactItem header='WhatsApp' contact='+393516822303' href='https://wa.me/393516822303'>
            <FaWhatsapp className='text-liBg group-hover:text-liSec dark:text-primary dark:group-hover:text-primary m-auto text-2xl mb-4' />
          </ContactItem>
        </div>

        <form noValidate autoComplete='off' ref={form} 
          onSubmit={handleSubmit(sendEmail)} 
          className='flex flex-col gap-2 items-start w-full p-6 bg-liSec dark:bg-transparent rounded-2xl 
                    transition ease-linear duration-300 text-liSec dark:text-white'>
          <div className='form-control w-full'>
            <input
              {...register("name", { required: true })}
              type='text'
              name='name'
              placeholder='Your full name'
              aria-invalid={errors.name ? 'true' : 'false'}
              className={errors.name ? 'input input-error bg-liBg text-black' : 'input !outline-primaryAlt bg-liBg text-black'}
            />
            <div className='flex'>
              {errors.name?.type === 'required' &&
                <Error>Please enter a name</Error>}
              <span className='mt-1 label-text-alt text-xs'>&nbsp;</span>
            </div>
          </div>

          <div className='form-control w-full'>
            <input
              {...register("email", { required: true, pattern: /^\S+@\S+$/i })}
              type='email'
              name='email'
              placeholder='Your email'
              className={errors.email ? 'input input-error bg-liBg text-black' : 'input !outline-primaryAlt bg-liBg text-black'}
            />
            <div className='flex'>
              {errors.email?.type === 'required' &&
                <Error>Please enter a email</Error>}
              {errors.email?.type === 'pattern' &&
                <Error>Please enter a valid email</Error>}
              <span className='mt-1 label-test-alt text-xs'>&nbsp;</span>

            </div>
          </div>

          <div className='form-control w-full'>
            <textarea
              {...register("message", { required: true })}
              name='message'
              placeholder='Your message'
              rows='7'
              className={errors.message ? 'textarea textarea-error resize-none bg-liBg text-black' : 
                'textarea resize-none !outline-primaryAlt bg-liBg text-black'}
            ></textarea>
            <div className='flex'>
              {errors.message?.type === 'required' &&
                <Error>Please enter a message</Error>}
              <span className='mt-1 label-test-alt text-xs'>&nbsp;</span>
            </div>
          </div>

          <div className='flex justify-between w-full items-center text-liBg dark:text-white'>
            <div className='flex flex-col basis-1/3'>
              <h3 className='mb-1 text-center'>Rate My Portfolio</h3>
              <input type="range"
                name='rating'
                min="1" max="5"
                value={rangeValue}
                onChange={(e) => setRangeValue(e.target.value)}
                className="range range-accent text-black"
                step="1" />
              <div className="w-full flex justify-between text-xs px-2">
                <span>|</span>
                <span>|</span>
                <span>|</span>
                <span>|</span>
                <span>|</span>
              </div>
            </div>
            <button submit={"submit"}>Send</button>
          </div>
          <ToastContainer theme='dark' toastStyle={{ backgroundColor: '#1d2021', }} />
        </form>
      </div>
    </section>
  )
}

export default Contact