import React, { useRef } from 'react';
import emailjs from '@emailjs/browser';

export default function ContactUs() {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs.sendForm('service_1e76akw', 'template_czdfv8s', form.current, 'U7JJdf3f0XNJprWar')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });
  };

  return (
    <form ref={form} onSubmit={sendEmail} className=" p-2 m-2 ">
      <label className="m-2">Name</label>
      <input type="text" name="user_name" className="text-black" />
      <label className="m-2">Email</label>
      <input type="email" name="user_email" className="text-black" />
      <label className="m-2">Message</label>
      <textarea name="message" className="text-black p-2 " />
      <input type="submit" value="Send" className="text-black p-2 border-solid border-2 border-white-500 mt-4 hover:border-[#243c5a] cursor-pointer " />
    </form>
  );
};